﻿Imports System.Data.SqlClient
Public Class FrmSelServicios

    Private Sub Button5_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button5.Click

        Try
            'PreguntaPlanForzoso(CInt(Contrato))
            If IsNumeric(ComboBox1.SelectedValue) = True Then
                GloGuardarNet = False
                If GloClv_TipSer <> 5 Then
                    GloClv_Servicio = ComboBox1.SelectedValue
                    locGloClv_servicioPromocion = GloClv_Servicio
                Else
                    GloClv_servTel = Me.ComboBox1.SelectedValue
                    locGloClv_servicioPromocion = GloClv_servTel
                End If

                'If UCase(Pantalla) = UCase("FrmClientes") Then
                '    If GloClv_Servicio > 0 Then
                '        Me.Validate()
                '        Me.ConsultaclientesnetTableAdapter1.Insert(Contrato, "C", 0, 0, False, False, 0, "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", 0, "", False, LoContratonet)
                '        Me.ConsultaclientesnetTableAdapter1.Update(Me.NewSofTvDataSet.CONSULTACLIENTESNET)
                '        Me.ConsultacontnetTableAdapter1.Insert(LoContratonet, GloClv_Servicio, "C", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", "01/01/1900", True, 0, 0, False, "C", "", True, "", 0, 0, "", "", LoClv_Unicanet)
                '        Me.ConsultacontnetTableAdapter1.Update(Me.NewSofTvDataSet.CONSULTACONTNET)
                '        FrmClientes.creaarbol()
                '        FrmClientes.Panel5.Visible = False
                '        FrmClientes.Panel6.Visible = True
                '        FrmClientes.VerAparatodelClienteTableAdapter.Fill(Me.NewSofTvDataSet.VerAparatodelCliente, CType(LoContratonet, Long))
                '        FrmClientes.CONSULTACLIENTESNETTableAdapter.FillCLIENTESNET(Me.NewSofTvDataSet.CONSULTACLIENTESNET, Contrato, CType(LoContratonet, Long))
                '        FrmClientes.CONSULTACONTNETTableAdapter.Fill(Me.NewSofTvDataSet.CONSULTACONTNET, New System.Nullable(Of Long)(CType(LoClv_Unicanet, Long)))
                '        FrmClientes.CREAARBOL()

                '        GloClv_Servicio = 0
                '    End If
                If GloClv_TipSer = 2 Then
                    If Equip_tel = True Then
                        numtel = True
                    End If
                    GloBndSer = True
                ElseIf GloClv_TipSer = 5 Then
                    globndTel = True
                End If
                Me.Close()
                frmctr.Activar()
            Else
                MsgBox("No a Seleccionado un Servicio", MsgBoxStyle.Information)
            End If

            If PreguntaPlan = 1 Then
                If MessageBox.Show("¿Deseas contratar Plan Forzoso?", "Atención", MessageBoxButtons.YesNo) = Windows.Forms.DialogResult.Yes Then
                    PlanForzoso(CInt(Contrato))
                End If
            End If
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        GloClv_Servicio = 0
        GloClv_servTel = 0
        Me.Close()
    End Sub

    Private Sub FrmSelServicios_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        locGloClv_servicioPromocion = 0
        frmctr.MdiParent = FrmClientes
        Try
            Dim CON As New SqlConnection(MiConexion)
            CON.Open()
            Me.MuestraServiciosTableAdapter.Connection = CON
            Me.MuestraServiciosTableAdapter.Fill(Me.NewSofTvDataSet.MuestraServicios, New System.Nullable(Of Integer)(CType(GloClv_TipSer, Long)))
            CON.Close()
        Catch ex As System.Exception
            System.Windows.Forms.MessageBox.Show(ex.Message)
        End Try
    End Sub
    Private Sub PlanForzoso(ByVal Contrato)
        BaseII.limpiaParametros()
        BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
        BaseII.CreateMyParameter("@MSJ", ParameterDirection.Output, SqlDbType.VarChar, 150)
        BaseII.ProcedimientoOutPut("PlanForzoso")
        eMsj = ""
        eMsj = BaseII.dicoPar("@MSJ").ToString
        MessageBox.Show(eMsj)
    End Sub
    'Private Sub PreguntaPlanForzoso(ByVal Contrato)
    '    BaseII.limpiaParametros()
    '    BaseII.CreateMyParameter("@Contrato", SqlDbType.Int, Contrato)
    '    BaseII.CreateMyParameter("@clv_servicio", SqlDbType.Int, 2)
    '    BaseII.CreateMyParameter("@BND", ParameterDirection.Output, SqlDbType.VarChar, 2)
    '    BaseII.ProcedimientoOutPut("MandaPlanForzoso")
    '    PreguntaPlan = ""
    '    PreguntaPlan = BaseII.dicoPar("@BND").ToString

    'End Sub
    Private Sub PreguntaPlanForzoso(ByVal Contrato)
        Dim CON As New SqlConnection(MiConexion)
        Dim CMD As New SqlCommand("MandaPlanForzoso", CON)
        CMD.CommandType = CommandType.StoredProcedure

        Dim PRM1 As New SqlParameter("@Contrato", SqlDbType.Int)
        PRM1.Direction = ParameterDirection.Input
        PRM1.Value = Contrato
        CMD.Parameters.Add(PRM1)

        Dim PRM2 As New SqlParameter("@clv_servicio", SqlDbType.Int)
        PRM2.Direction = ParameterDirection.Input
        PRM2.Value = 2
        CMD.Parameters.Add(PRM2)

        Dim PRM3 As New SqlParameter("@BND", SqlDbType.Int)
        PRM3.Direction = ParameterDirection.Output
        CMD.Parameters.Add(PRM3)

        Try
            CON.Open()
            CMD.ExecuteNonQuery()
            PreguntaPlan = PRM3.Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Information)
        Finally
            CON.Close()
            CON.Dispose()
        End Try
    End Sub
End Class