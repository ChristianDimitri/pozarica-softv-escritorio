Imports System.Data.SqlClient
Imports System.Text
Public Class FrmRespuestas


    Function ConRespuestasNombre(ByVal IDRespuesta As Integer, ByVal Op As Integer) As DataTable
        Dim conexion As New SqlConnection(MiConexion)
        Dim strSQL As New StringBuilder()
        strSQL.Append("EXEC ConRespuestasNombre ")
        strSQL.Append(CStr(IDRespuesta) & ", ")
        strSQL.Append(CStr(Op))
        Dim dataAdapter As New SqlDataAdapter(strSQL.ToString(), conexion)
        Dim dataTable As New DataTable

        Try
            dataAdapter.Fill(dataTable)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Exclamation)
        End Try

        Return dataTable

    End Function

    Private Sub LlenaTreeViewRespuestas()
        Dim dataTableAparatos As New DataTable
        Dim dataTableServicios As New DataTable
        Dim x As Integer = 0
        Dim y As Integer = 0

        TreeViewRespuestas.Nodes.Clear()
        dataTableAparatos = ConRespuestasNombre(0, 0)

        For Each RowAparato As DataRow In dataTableAparatos.Rows

            TreeViewRespuestas.Nodes.Add(RowAparato("RESPUESTA").ToString())
            TreeViewRespuestas.Nodes(x).Tag = RowAparato("IDPREGUNTA").ToString()
            dataTableServicios = ConRespuestasNombre(RowAparato("IDPREGUNTA").ToString(), 1)

            For Each RowServicio As DataRow In dataTableServicios.Rows
                TreeViewRespuestas.Nodes(x).Nodes.Add(RowServicio("RESPUESTA").ToString()).ForeColor = Color.Blue
                TreeViewRespuestas.Nodes(x).Nodes(y).Tag = RowServicio("IDPREGUNTA").ToString()
                y += 1
            Next

            TreeViewRespuestas.Nodes(x).ExpandAll()
            x += 1
            y = 0
        Next
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        eBndRespuesta = True
        Me.Close()
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        eBndRespuesta = False
        Me.Close()
    End Sub

    Private Sub FrmRespuestas_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        colorea(Me, Me.Name)
        eBndRespuesta = False
        eIDPreguntaAux = 0
        LlenaTreeViewRespuestas()
    End Sub

    Private Sub TreeViewRespuestas_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeViewRespuestas.AfterSelect
        If IsNumeric(e.Node.Tag) = False Then
            Exit Sub
        End If

        eIDPreguntaAux = CInt(e.Node.Tag)

    End Sub
End Class