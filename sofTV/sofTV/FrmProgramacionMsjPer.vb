Imports System.Data.SqlClient

Public Class FrmProgramacionMsjPer

    Dim contador, DIAS, dias_semana, habilitar, tip_prog, tip_frec, tip_inter, INTERVALO, ordinal, unidades, fecha_inicio, fecha_final, hora_inicio, hora_final As Integer
    Public actualizar As Boolean = False
    Public nombre As String = ""

    Private Sub FrmProgramacionMsjPer_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        rMensajes = False
    End Sub

    Private Sub FrmProgramacionMsjPer_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        colorea(Me, Me.Name)
        LocClv_session = rSession
        'TODO: esta l�nea de c�digo carga datos en la tabla 'DataSetLidia.Muestra_Programacion' Puede moverla o quitarla seg�n sea necesario.
        'Me.Muestra_ProgramacionTableAdapter.Connection = con
        'Me.Muestra_ProgramacionTableAdapter.Fill(Me.DataSetLidia.Muestra_Programacion)
        Me.ComboBox3.DataSource = Me.weekDay()
        Me.ComboBox2.DataSource = Me.Ordinales()
        Me.ComboBox4.DataSource = Me.MuestraIntervalos()
        Me.cbTipMensaje.DataSource = Me.TipoMensajes()
        If Me.actualizar = False Then
            Me.datosOriginales()
        Else
            Me.GroupBox1.Enabled = False
            Me.GroupBox2.Enabled = False
            Me.GroupBox3.Enabled = False
            Me.datosActualizar(Me.nombre)
        End If
    End Sub



    Private Sub RadioButton1_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton1.CheckedChanged
        If Me.RadioButton1.Checked = True Then
            Me.NumericUpDown3.Enabled = True
            Me.NumericUpDown4.Enabled = True
            Me.Label10.Enabled = True
            Me.Label9.Enabled = True
            tip_frec = 16
        Else
            Me.NumericUpDown3.Enabled = False
            Me.NumericUpDown4.Enabled = False
            Me.Label10.Enabled = False
            Me.Label9.Enabled = False
        End If
    End Sub

    Private Sub RadioButton2_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton2.CheckedChanged
        If Me.RadioButton2.Checked = True Then
            Me.ComboBox2.Enabled = True
            Me.ComboBox3.Enabled = True
            Me.NumericUpDown5.Enabled = True
            Me.Label11.Enabled = True
            Me.Label12.Enabled = True
            tip_frec = 32
        Else
            Me.ComboBox2.Enabled = False
            Me.ComboBox3.Enabled = False
            Me.NumericUpDown5.Enabled = False
            Me.Label11.Enabled = False
            Me.Label12.Enabled = False
        End If
    End Sub

    Private Sub Button1_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button1.Click
        If Me.cbTipMensaje.SelectedValue > 0 Then
            If Me.cbNombre.SelectedValue > 0 Then
                If tip_frec = 1 Then
                    DateTimePicker2.Format = DateTimePickerFormat.Custom
                    DateTimePicker2.CustomFormat = "yyyyMMdd"
                    fecha_inicio = CInt(DateTimePicker2.Text)
                    DateTimePicker2.Format = DateTimePickerFormat.Short

                    DateTimePicker1.Format = DateTimePickerFormat.Custom
                    DateTimePicker1.CustomFormat = "HHmmss"
                    hora_inicio = CInt(DateTimePicker1.Text)
                    DateTimePicker1.Format = DateTimePickerFormat.Time
                    If Me.actualizar = False Then
                        Me.guardaMensaje()
                    Else
                        job = Me.cbNombre.Text.ToString + " " + DateTime.Today.Now.ToString
                        Me.ActualizaTrabajo(job, tip_frec, DIAS, tip_inter, ordinal, INTERVALO, unidades, fecha_inicio, fecha_final, hora_inicio, hora_final)
                        MsgBox("La Programaci�n fue Grabada con �xito", MsgBoxStyle.Information)
                    End If
                ElseIf tip_frec = 0 Then
                    MsgBox("Se Debe de de Seleccionar una Programaci�n ", MsgBoxStyle.Information)
                    Exit Sub
                ElseIf tip_frec = 4 Then  ' DIAS    ====================
                    DIAS = Me.NumericUpDown1.Value
                ElseIf tip_frec = 8 Then  ' SEMANAS   =============
                    unidades = Me.NumericUpDown2.Value
                    Checa_dias(Me.TabPage3)
                    If dias_semana = 0 Then
                        MsgBox("Se debe de Seleccionar por lo Menos un Dia de la Semana")
                        Exit Sub
                    End If
                    DIAS = dias_semana
                ElseIf tip_frec = 32 Then  ' MESES    ===============
                    unidades = Me.NumericUpDown5.Value
                    DIAS = Me.ComboBox3.SelectedValue
                    ordinal = Me.ComboBox2.SelectedValue
                ElseIf tip_frec = 16 Then
                    DIAS = Me.NumericUpDown3.Value
                    unidades = Me.NumericUpDown4.Value
                End If

                If Me.RadioButton3.Checked = False And Me.RadioButton4.Checked = True And tip_frec > 1 Then
                    DateTimePicker3.Format = DateTimePickerFormat.Custom
                    DateTimePicker3.CustomFormat = "HHmmss"
                    hora_inicio = CInt(Me.DateTimePicker3.Text)
                    DateTimePicker3.Format = DateTimePickerFormat.Time
                ElseIf Me.RadioButton3.Checked = True And Me.RadioButton4.Checked = False Then
                    If Me.ComboBox4.Text = " " Then
                        MsgBox("Seleccione Horas/Minutos en el Combo ")
                    End If
                    INTERVALO = Me.NumericUpDown6.Value
                    DateTimePicker4.Format = DateTimePickerFormat.Custom
                    DateTimePicker4.CustomFormat = "HHmmss"
                    Dim hora As String = DateTime.Now.Hour.ToString + DateTime.Now.Minute.ToString + DateTime.Now.Second.ToString
                    If Me.DateTimePicker8.Text = DateTime.Now.ToShortDateString Then
                        If Integer.Parse(hora) > Integer.Parse(Me.DateTimePicker4.Text) Then
                            hora_inicio = CInt(Me.DateTimePicker4.Text)
                        Else
                            hora_inicio = hora + 10000
                        End If
                    End If
                    DateTimePicker4.Format = DateTimePickerFormat.Time

                    DateTimePicker5.Format = DateTimePickerFormat.Custom
                    DateTimePicker5.CustomFormat = "HHmmss"
                    hora_final = CInt(Me.DateTimePicker5.Text)
                    DateTimePicker5.Format = DateTimePickerFormat.Time
                    tip_inter = Me.ComboBox4.SelectedValue

                    End If

                    DateTimePicker8.Format = DateTimePickerFormat.Custom
                    DateTimePicker8.CustomFormat = "yyyyMMdd"
                    fecha_inicio = CInt(Me.DateTimePicker8.Text)
                    DateTimePicker8.Format = DateTimePickerFormat.Short

                    If Me.RadioButton6.Checked = True Then
                        DateTimePicker7.Format = DateTimePickerFormat.Custom
                        DateTimePicker7.CustomFormat = "yyyyMMdd"
                        fecha_final = CInt(Me.DateTimePicker7.Text)
                        DateTimePicker7.Format = DateTimePickerFormat.Short
                    End If
                    If Me.actualizar = False Then
                        Me.guardaMensaje()
                    Else
                        job = Me.cbNombre.Text.ToString + " " + DateTime.Today.Now.ToString
                        Me.ActualizaTrabajo(job, tip_frec, DIAS, tip_inter, ordinal, INTERVALO, unidades, fecha_inicio, fecha_final, hora_inicio, hora_final)
                        MsgBox("La Programaci�n fue Grabada con �xito", MsgBoxStyle.Information)
                    End If
            Else
                MsgBox("Debe seleccionar un mensaje para continuar", MsgBoxStyle.Information)
            End If
        Else
            MsgBox("Debe seleccionar un tipo de mensaje para continuar", MsgBoxStyle.Information)
        End If
    End Sub

    Private Sub guardaMensaje()
        job = Me.cbNombre.Text.ToString + " " + DateTime.Today.Now.ToString
        descrip_job = ""
        name_prog = job
        If Me.CreaJob(job, descrip_job) = True Then
            If Me.creaPasos(job, GloDatabaseName) = True Then
                If Me.creaProgramacion(job, name_prog, habilitar, DIAS, tip_frec, tip_inter, INTERVALO, ordinal, unidades, fecha_inicio, fecha_final, hora_inicio, hora_final, LocClv_session) = True Then
                    MsgBox("La Programaci�n fue Grabada con �xito", MsgBoxStyle.Information)
                End If
            End If
        Else
            MsgBox("No se grab� la programacion correctamente", MsgBoxStyle.Critical)
        End If
        bitsist(GloUsuario, 0, LocGloSistema, Me.Name, "Se Programo un Mensaje:" + job, "Fecha Inicial:" + Me.DateTimePicker2.Text + " Fecha Final: " + Me.DateTimePicker7.Text, "Hora Inicial: " + CStr(hora_inicio) + " Hora Final: " + CStr(hora_final), LocClv_Ciudad)
        bec_bnd = True
        habilitar = 0
        tip_prog = 0
        tip_frec = 0
        tip_inter = 0
        DIAS = 0
        INTERVALO = 0
        ordinal = 0
        unidades = 0
        fecha_inicio = 0
        fecha_final = 99991231
        hora_inicio = 0
        hora_final = 235959
        Programacion = 0
        dias_semana = 0
        FrmGuardaMensaje.Close()
        FrmSelEstado_Mensajes.Close()
        Me.Close()
        Module1.rMensajes = False
    End Sub

    Private Sub RadioButton4_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton4.CheckedChanged
        If Me.RadioButton4.Checked = False Then
            Me.DateTimePicker3.Enabled = False
            Me.NumericUpDown6.Enabled = True
            Me.ComboBox4.Enabled = True
            Me.DateTimePicker4.Enabled = True
            Me.DateTimePicker5.Enabled = True
        ElseIf Me.RadioButton4.Checked = True Then
            Me.DateTimePicker3.Enabled = True
            Me.NumericUpDown6.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.DateTimePicker4.Enabled = False
            Me.DateTimePicker5.Enabled = False

        Else
            Me.DateTimePicker3.Enabled = False
            Me.NumericUpDown6.Enabled = False
            Me.ComboBox4.Enabled = False
            Me.DateTimePicker4.Enabled = False
            Me.DateTimePicker5.Enabled = False
        End If
    End Sub

    Public Sub Checa_dias(ByVal formulario As TabPage) 'tab page
        Dim panel3 As Panel
        Dim label As Label
        Dim var As Integer = 0
        Dim TEXT As TextBox
        Dim CHECK As CheckBox
        Dim RADIO As RadioButton
        Dim boton As Button

        For Each ctm As Control In formulario.Controls
            If ctm.GetType Is GetType(System.Windows.Forms.CheckBox) Then
                CHECK = New CheckBox
                CHECK = ctm
                If CHECK.Checked = True Then
                    If dias_semana = 0 Then
                        var = CInt(Mid(CHECK.Name, 9, 1))
                        var = 2 ^ (var - 1)
                        dias_semana = var
                    Else
                        var = CInt(Mid(CHECK.Name, 9, 1))
                        var = 2 ^ (var - 1)
                        dias_semana = dias_semana + var
                    End If
                End If
                CHECK = Nothing
            End If
        Next
    End Sub

    Private Sub Button2_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub

    Private Sub CheckBox8_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles CheckBox8.CheckedChanged
        If Me.CheckBox8.Checked = True Then
            habilitar = 1
        Else
            habilitar = 0
        End If
    End Sub

    Private Sub RadioButton6_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton6.CheckedChanged
        If Me.RadioButton6.Checked = False Then
            Me.DateTimePicker7.Enabled = False
        ElseIf Me.RadioButton6.Enabled = True Then
            Me.DateTimePicker7.Enabled = True
        End If
    End Sub

    Private Sub RadioButton5_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles RadioButton5.CheckedChanged
        If Me.RadioButton5.Checked = False Then
            Me.DateTimePicker7.Enabled = True
        ElseIf Me.RadioButton5.Checked = True Then
            Me.DateTimePicker7.Enabled = False
        End If
    End Sub

    Private Sub TabControl1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles TabControl1.SelectedIndexChanged
        Dim I As Integer = Me.TabControl1.SelectedIndex

        Select Case I
            Case 0
                Me.DateTimePicker1.Visible = True
                Me.DateTimePicker2.Visible = True
                Me.GroupBox2.Enabled = False
                Me.GroupBox3.Enabled = False
                Me.Label2.Visible = True
                Me.Label3.Visible = True
                tip_frec = 1
            Case 1
                Me.DateTimePicker1.Visible = False
                Me.DateTimePicker2.Visible = False
                Me.Label2.Visible = False
                Me.Label3.Visible = False
                Me.GroupBox2.Enabled = True
                Me.GroupBox3.Enabled = True
                Me.TabControl1.Enabled = True
                tip_frec = 4
            Case 2

                Me.DateTimePicker1.Visible = False
                Me.DateTimePicker2.Visible = False
                Me.Label2.Visible = False
                Me.Label3.Visible = False
                Me.GroupBox2.Enabled = True
                Me.GroupBox3.Enabled = True
                tip_frec = 8
            Case 3
                Me.RadioButton1.Checked = True
                If Me.RadioButton1.Checked = True Then
                    Me.NumericUpDown5.Enabled = False
                    Me.ComboBox2.Enabled = False
                    Me.ComboBox3.Enabled = False
                    tip_frec = 16
                Else
                    Me.NumericUpDown4.Enabled = False
                    Me.NumericUpDown3.Enabled = False
                    tip_frec = 32
                End If
                Me.DateTimePicker1.Visible = False
                Me.DateTimePicker2.Visible = False
                Me.Label2.Visible = False
                Me.Label3.Visible = False
                Me.GroupBox2.Enabled = True
                Me.GroupBox3.Enabled = True
        End Select
    End Sub

    Private Function TipoMensajes() As BindingSource
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("SELECT 0 As Clv_Tmensaje, '--Selecciona--' As Tipo_Mensaje Union SELECT Clv_Tmensaje,Tipo_Mensaje FROM TipoMensaje", con)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.Text
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

    Private Function Mensajes(ByVal clv As Integer) As BindingSource
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("Select 0 As ClvMensaje, '--Selecciona--' As Descripcion Union Select ClvMensaje,Descripcion From MensajePersonal Where Clv_TMensaje=" + clv.ToString, con)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.Text
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

    Private Function weekDay() As BindingSource
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("Exec Muestra_WeekDay", con)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.Text
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

    Private Function Ordinales() As BindingSource
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("Exec Muestra_Ordinales", con)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.Text
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

    Private Function MuestraIntervalos() As BindingSource
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("Exec Muestra_TipoIntervalo", con)
        Dim tabla As DataTable = New DataTable
        Dim bs As BindingSource = New BindingSource
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.Text
        Try
            con.Open()
            da.Fill(tabla)
            bs.DataSource = tabla
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        Return bs
    End Function

    Private Function CreaJob(ByVal job As String, ByVal descripcion As String) As Boolean
        Dim regresa As Boolean = False
        Dim con As New SqlClient.SqlConnection(MiConexion)
        Dim com As New SqlClient.SqlCommand("Crea_Trabajo", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        com.Parameters.Add(New SqlParameter("@Job", job))
        com.Parameters.Add(New SqlParameter("@Descrip", descripcion))
        Try
            con.Open()
            com.ExecuteNonQuery()
            regresa = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            regresa = False
        Finally
            con.Close()
        End Try
        Return regresa
    End Function

    Private Function creaPasos(ByVal job As String, ByVal base As String) As Boolean
        Dim regresa As Boolean = False
        Dim comando As String = "Exec Manda_MsjsPersonalizadosProgramados " + LocClv_session.ToString + "," + Me.cbNombre.SelectedValue.ToString
        Dim paso As String = "Paso 1"
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("Crea_Pasos_sqlPERSONALIZADO", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        com.Parameters.Add(New SqlParameter("@Job", job))
        com.Parameters.Add(New SqlParameter("@Paso_name", paso))
        com.Parameters.Add(New SqlParameter("@Comando", comando))
        com.Parameters.Add(New SqlParameter("@bdd", base))
        Try
            con.Open()
            com.ExecuteNonQuery()
            regresa = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            regresa = False
        Finally
            con.Close()
        End Try
        Return regresa
    End Function

    Private Function creaProgramacion(ByVal job As String, ByVal nameProg As String, ByVal habilitar As Integer, ByVal dias As String, ByVal tipoFrec As Integer, ByVal tipoInter As Integer, ByVal intervalo As Integer, ByVal ordinal As Integer, ByVal unidades As Integer, ByVal fechaInicio As Integer, ByVal fechaFinal As Integer, ByVal horaInicio As Integer, ByVal horaFinal As Integer, ByVal clvSession As Long) As Boolean
        Dim regresa As Boolean = False
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("Crea_ProgramacionSqlPersonalizados", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        com.Parameters.Add(New SqlParameter("@Job", job))
        com.Parameters.Add(New SqlParameter("@Prog_name", nameProg))
        com.Parameters.Add(New SqlParameter("@Habilitar", habilitar))
        com.Parameters.Add(New SqlParameter("@Dias", dias))
        com.Parameters.Add(New SqlParameter("@Tip_frec", tipoFrec))
        com.Parameters.Add(New SqlParameter("@Tip_inter", tipoInter))
        com.Parameters.Add(New SqlParameter("@Intervalo", intervalo))
        com.Parameters.Add(New SqlParameter("@Ordinal", ordinal))
        com.Parameters.Add(New SqlParameter("@Unidades", unidades))
        com.Parameters.Add(New SqlParameter("@Fecha_inicio", fechaInicio))
        com.Parameters.Add(New SqlParameter("@Fecha_final", fechaFinal))
        com.Parameters.Add(New SqlParameter("@Hora_inicio", horaInicio))
        com.Parameters.Add(New SqlParameter("@Hora_final", horaFinal))
        com.Parameters.Add(New SqlParameter("@Clv_session", clvSession))
        Try
            con.Open()
            com.ExecuteNonQuery()
            regresa = True
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
            regresa = False
        Finally
            con.Close()
        End Try
        Return regresa
    End Function

    Private Function checaProg(ByVal nombre As String) As Integer
        Dim regresa As Integer
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("checar_progs", con)
        com.CommandType = CommandType.StoredProcedure
        com.CommandTimeout = 0
        com.Parameters.Add(New SqlParameter("@Nombre", nombre))
        com.Parameters.Add(New SqlParameter("@cont", SqlDbType.Int))
        com.Parameters("@cont").Direction = ParameterDirection.Output
        com.Parameters("@cont").Value = 0
        Try
            con.Open()
            com.ExecuteNonQuery()
            regresa = com.Parameters("@cont").Value
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        Return regresa
    End Function

    Private Function MuestraMensaje(ByVal clv As Integer) As String
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("Select Mensaje From MensajePersonal Where ClvMensaje=" + clv.ToString, con)
        Dim tabla As DataTable = New DataTable
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        Dim mensaje As String = " "
        com.CommandType = CommandType.Text
        Try
            con.Open()
            da.Fill(tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        If tabla.Rows.Count > 0 Then
            mensaje = tabla.Rows(0)(0).ToString
        End If
        Return mensaje
    End Function

    Private Sub cbNombre_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbNombre.SelectedIndexChanged
        Me.GroupBox1.Enabled = True
        Dim clv As Integer = Integer.Parse(Me.cbNombre.SelectedValue.ToString)
        Me.lblMensaje.Text = Me.MuestraMensaje(clv)
        If Me.cbTipMensaje.SelectedValue = 1 Then
            Me.TabControl1.SelectedTab = Me.TabPage2
            Me.TabControl1.Enabled = False
        Else
            Me.TabControl1.Enabled = True
            Me.TabControl1.SelectedTab = Me.TabPage1
        End If
    End Sub

    Private Sub cbTipMensaje_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles cbTipMensaje.SelectedIndexChanged
        Dim clv As Integer = Integer.Parse(Me.cbTipMensaje.SelectedValue.ToString)
        Me.cbNombre.DataSource = Me.Mensajes(clv)
    End Sub

    Private Sub datosOriginales()
        habilitar = 0
        tip_prog = 0
        tip_frec = 0
        tip_inter = 0
        DIAS = 0
        INTERVALO = 0
        ordinal = 0
        unidades = 0
        fecha_inicio = 0
        fecha_final = 99991231
        hora_inicio = 0
        hora_final = 235959

        Me.TabControl1.SelectedIndex = 0
        Me.GroupBox2.Enabled = False
        Me.GroupBox3.Enabled = False

        Me.CheckBox8.Checked = True
        Me.DateTimePicker7.Enabled = False
        Me.NumericUpDown6.Enabled = False
        Me.DateTimePicker3.Enabled = False
        Me.ComboBox4.Enabled = False
        Me.DateTimePicker4.Enabled = False
        Me.DateTimePicker5.Enabled = False
        Me.RadioButton5.Checked = True
        Me.RadioButton4.Checked = True
    End Sub

    Private Sub regresaTiposMensaje(ByVal nombre As String)
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("regresaTiposMensaje", con)
        Dim tabla As DataTable = New DataTable
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@nombre", nombre))
        Try
            con.Open()
            da.Fill(tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        If tabla.Rows.Count > 0 Then
            Me.cbTipMensaje.SelectedValue = Integer.Parse(tabla.Rows(0)(1).ToString)
            Me.cbNombre.SelectedValue = Integer.Parse(tabla.Rows(0)(0).ToString)
        End If
    End Sub

    Public Sub datosActualizar(ByVal nombre As String)
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("BuscaProgPersonalizados", con)
        Dim tabla As DataTable = New DataTable
        Dim da As SqlDataAdapter = New SqlDataAdapter(com)
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@Nombre", nombre))
        com.Parameters.Add(New SqlParameter("@fecha", 0))
        com.Parameters.Add(New SqlParameter("@op", 4))
        Try
            con.Open()
            da.Fill(tabla)
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
        Me.regresaTiposMensaje(nombre)
        
        If tabla.Rows.Count > 0 Then
            Me.CheckBox8.Checked = Integer.Parse(tabla.Rows(0)(0).ToString)
            tip_frec = Integer.Parse(tabla.Rows(0)(1).ToString)
            DIAS = Integer.Parse(tabla.Rows(0)(2).ToString)
            tip_inter = Integer.Parse(tabla.Rows(0)(3).ToString)
            ordinal = Integer.Parse(tabla.Rows(0)(4).ToString)
            INTERVALO = Integer.Parse(tabla.Rows(0)(5).ToString)
            unidades = Integer.Parse(tabla.Rows(0)(6).ToString)
            fecha_inicio = Integer.Parse(tabla.Rows(0)(7).ToString)
            fecha_final = Integer.Parse(tabla.Rows(0)(8).ToString)
            hora_inicio = Integer.Parse(tabla.Rows(0)(9).ToString)
            hora_final = Integer.Parse(tabla.Rows(0)(10).ToString)
        Else
            Return
        End If
        Dim horas As String
        Select Case tip_frec
            Case 1
                Me.DateTimePicker2.Text = CDate(Mid(fecha_inicio, 7, 2) + "/" + Mid(fecha_inicio, 5, 2) + "/" + Mid(fecha_inicio, 1, 4))
                horas = hora_inicio.ToString.PadLeft(6, "0")
                Me.DateTimePicker1.Text = CDate(Mid(horas, 1, 2) + ":" + Mid(horas, 3, 2) + ":" + Mid(horas, 5, 2))
                Me.TabControl1.SelectedIndex = 0
            Case 4
                Me.NumericUpDown1.Value = DIAS
                Me.TabControl1.SelectedIndex = 1
            Case 8
                Me.NumericUpDown2.Value = unidades
                Checa_dias2(INTERVALO)
                Me.TabControl1.SelectedIndex = 2
            Case 16
                Me.NumericUpDown4.Value = unidades
                Me.NumericUpDown3.Value = INTERVALO
                Me.RadioButton1.Checked = True
                Me.TabControl1.SelectedIndex = 3
            Case 32
                Me.NumericUpDown5.Value = unidades
                Me.ComboBox2.SelectedValue = INTERVALO
                Me.ComboBox3.SelectedValue = DIAS
                Me.RadioButton2.Checked = True
                Me.TabControl1.SelectedIndex = 3
        End Select
        If tip_inter = 1 Then
            horas = hora_inicio.ToString.PadLeft(6, "0")
            Me.DateTimePicker3.Text = CDate(Mid(horas, 1, 2) + ":" + Mid(horas, 3, 2) + ":" + Mid(horas, 5, 2))
            Me.RadioButton4.Checked = True
            Me.RadioButton4.Checked = True
        ElseIf tip_inter > 1 Then
            horas = hora_inicio.ToString.PadLeft(6, "0")
            Me.NumericUpDown6.Value = ordinal
            Me.ComboBox4.SelectedValue = tip_inter
            Me.RadioButton3.Checked = True
            Me.DateTimePicker4.Text = CDate(Mid(horas, 1, 2) + ":" + Mid(horas, 3, 2) + ":" + Mid(horas, 5, 2))
            horas = hora_final.ToString.PadLeft(6, "0")
            Me.DateTimePicker5.Text = CDate(Mid(horas, 1, 2) + ":" + Mid(horas, 3, 2) + ":" + Mid(horas, 5, 2))
        End If

        Me.DateTimePicker8.Value = CDate(Mid(fecha_inicio, 7, 2) + "/" + Mid(fecha_inicio, 5, 2) + "/" + Mid(fecha_inicio, 1, 4))

        If fecha_final = 99991231 Then
            Me.RadioButton5.Checked = True
        Else
            Me.RadioButton6.Checked = True
            Me.DateTimePicker7.Value = CDate(Mid(fecha_final, 7, 2) + "/" + Mid(fecha_final, 5, 2) + "/" + Mid(fecha_final, 1, 4))
        End If
    End Sub

    Private Sub Checa_dias2(ByVal numero As Integer) 'Panel
        If numero >= 64 Then
            Me.CheckBox6.Checked = True
            numero = numero - 64
        End If
        If numero >= 32 Then
            Me.CheckBox5.Checked = True
            numero = numero - 32
        End If
        If numero >= 16 Then
            Me.CheckBox4.Checked = True
            numero = numero - 16
        End If
        If numero >= 8 Then
            Me.CheckBox3.Checked = True
            numero = numero - 8
        End If
        If numero >= 4 Then
            Me.CheckBox2.Checked = True
            numero = numero - 4
        End If
        If numero >= 2 Then
            Me.CheckBox1.Checked = True
            numero = numero - 2
        End If
        If numero = 1 Then
            Me.CheckBox7.Checked = True
        End If
    End Sub

    Private Sub ActualizaTrabajo(ByVal nameJob As String, ByVal frecType As Integer, ByVal frecInter As Integer, ByVal frecSubType As Integer, ByVal frecSubInter As Integer, ByVal frecRelInter As Integer, ByVal frecRecFac As Integer, ByVal fechaI As Integer, ByVal fechaF As Integer, ByVal hraIn As Integer, ByVal hraFin As Integer)
        Dim con As SqlConnection = New SqlConnection(MiConexion)
        Dim com As SqlCommand = New SqlCommand("Modifica_ProgramacionPersonalizados", con)
        Dim tabla As DataTable = New DataTable
        com.CommandType = CommandType.StoredProcedure
        com.Parameters.Add(New SqlParameter("@namejob", nameJob))
        com.Parameters.Add(New SqlParameter("@frec_type", frecType))
        com.Parameters.Add(New SqlParameter("@frec_interval", frecInter))
        com.Parameters.Add(New SqlParameter("@frec_sub_type", frecSubType))
        com.Parameters.Add(New SqlParameter("@frec_sub_inter", frecSubInter))
        com.Parameters.Add(New SqlParameter("@frec_rel_inter", frecRelInter))
        com.Parameters.Add(New SqlParameter("@frec_rec_fac", frecRecFac))
        com.Parameters.Add(New SqlParameter("@fecha_ini", fechaI))
        com.Parameters.Add(New SqlParameter("@fecha_fin", fechaF))
        com.Parameters.Add(New SqlParameter("@hr_ini", hraIn))
        com.Parameters.Add(New SqlParameter("@hr_fin", hraFin))
        Try
            con.Open()
            com.ExecuteNonQuery()
        Catch ex As Exception
            MsgBox(ex.Message, MsgBoxStyle.Critical)
        Finally
            con.Close()
        End Try
    End Sub

End Class