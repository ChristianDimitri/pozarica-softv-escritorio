<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class BrwRangos
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim CveRangoLabel As System.Windows.Forms.Label
        Dim RangoInferiorLabel As System.Windows.Forms.Label
        Dim RangoSuperiorLabel As System.Windows.Forms.Label
        Dim DataGridViewCellStyle3 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle
        Me.Button1 = New System.Windows.Forms.Button
        Me.Button2 = New System.Windows.Forms.Button
        Me.Button3 = New System.Windows.Forms.Button
        Me.DataSetEDGAR = New sofTV.DataSetEDGAR
        Me.MuestraCatalogoDeRangosBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.MuestraCatalogoDeRangosTableAdapter = New sofTV.DataSetEDGARTableAdapters.MuestraCatalogoDeRangosTableAdapter
        Me.MuestraCatalogoDeRangosDataGridView = New System.Windows.Forms.DataGridView
        Me.DataGridViewTextBoxColumn1 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn2 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.DataGridViewTextBoxColumn3 = New System.Windows.Forms.DataGridViewTextBoxColumn
        Me.CveRangoTextBox = New System.Windows.Forms.TextBox
        Me.RangoInferiorTextBox = New System.Windows.Forms.TextBox
        Me.RangoSuperiorTextBox = New System.Windows.Forms.TextBox
        CveRangoLabel = New System.Windows.Forms.Label
        RangoInferiorLabel = New System.Windows.Forms.Label
        RangoSuperiorLabel = New System.Windows.Forms.Label
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraCatalogoDeRangosBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.MuestraCatalogoDeRangosDataGridView, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CveRangoLabel
        '
        CveRangoLabel.AutoSize = True
        CveRangoLabel.Location = New System.Drawing.Point(147, 162)
        CveRangoLabel.Name = "CveRangoLabel"
        CveRangoLabel.Size = New System.Drawing.Size(64, 13)
        CveRangoLabel.TabIndex = 13
        CveRangoLabel.Text = "Cve Rango:"
        '
        'RangoInferiorLabel
        '
        RangoInferiorLabel.AutoSize = True
        RangoInferiorLabel.Location = New System.Drawing.Point(134, 188)
        RangoInferiorLabel.Name = "RangoInferiorLabel"
        RangoInferiorLabel.Size = New System.Drawing.Size(77, 13)
        RangoInferiorLabel.TabIndex = 14
        RangoInferiorLabel.Text = "Rango Inferior:"
        '
        'RangoSuperiorLabel
        '
        RangoSuperiorLabel.AutoSize = True
        RangoSuperiorLabel.Location = New System.Drawing.Point(127, 214)
        RangoSuperiorLabel.Name = "RangoSuperiorLabel"
        RangoSuperiorLabel.Size = New System.Drawing.Size(84, 13)
        RangoSuperiorLabel.TabIndex = 15
        RangoSuperiorLabel.Text = "Rango Superior:"
        '
        'Button1
        '
        Me.Button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button1.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button1.Location = New System.Drawing.Point(588, 23)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(136, 36)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "&NUEVO"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button2.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button2.Location = New System.Drawing.Point(588, 65)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(136, 36)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "&MODIFICAR"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Button3.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Button3.Location = New System.Drawing.Point(588, 499)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(136, 36)
        Me.Button3.TabIndex = 2
        Me.Button3.Text = "&SALIR"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'DataSetEDGAR
        '
        Me.DataSetEDGAR.DataSetName = "DataSetEDGAR"
        Me.DataSetEDGAR.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'MuestraCatalogoDeRangosBindingSource
        '
        Me.MuestraCatalogoDeRangosBindingSource.DataMember = "MuestraCatalogoDeRangos"
        Me.MuestraCatalogoDeRangosBindingSource.DataSource = Me.DataSetEDGAR
        '
        'MuestraCatalogoDeRangosTableAdapter
        '
        Me.MuestraCatalogoDeRangosTableAdapter.ClearBeforeFill = True
        '
        'MuestraCatalogoDeRangosDataGridView
        '
        Me.MuestraCatalogoDeRangosDataGridView.AllowUserToAddRows = False
        Me.MuestraCatalogoDeRangosDataGridView.AllowUserToDeleteRows = False
        Me.MuestraCatalogoDeRangosDataGridView.AutoGenerateColumns = False
        Me.MuestraCatalogoDeRangosDataGridView.BackgroundColor = System.Drawing.Color.WhiteSmoke
        DataGridViewCellStyle3.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleCenter
        DataGridViewCellStyle3.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle3.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle3.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle3.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle3.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle3.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.MuestraCatalogoDeRangosDataGridView.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle3
        Me.MuestraCatalogoDeRangosDataGridView.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.DataGridViewTextBoxColumn1, Me.DataGridViewTextBoxColumn2, Me.DataGridViewTextBoxColumn3})
        Me.MuestraCatalogoDeRangosDataGridView.DataSource = Me.MuestraCatalogoDeRangosBindingSource
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.MuestraCatalogoDeRangosDataGridView.DefaultCellStyle = DataGridViewCellStyle4
        Me.MuestraCatalogoDeRangosDataGridView.Location = New System.Drawing.Point(12, 23)
        Me.MuestraCatalogoDeRangosDataGridView.Name = "MuestraCatalogoDeRangosDataGridView"
        Me.MuestraCatalogoDeRangosDataGridView.ReadOnly = True
        Me.MuestraCatalogoDeRangosDataGridView.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect
        Me.MuestraCatalogoDeRangosDataGridView.Size = New System.Drawing.Size(558, 521)
        Me.MuestraCatalogoDeRangosDataGridView.TabIndex = 13
        Me.MuestraCatalogoDeRangosDataGridView.TabStop = False
        '
        'DataGridViewTextBoxColumn1
        '
        Me.DataGridViewTextBoxColumn1.DataPropertyName = "CveRango"
        Me.DataGridViewTextBoxColumn1.HeaderText = "CveRango"
        Me.DataGridViewTextBoxColumn1.Name = "DataGridViewTextBoxColumn1"
        Me.DataGridViewTextBoxColumn1.ReadOnly = True
        '
        'DataGridViewTextBoxColumn2
        '
        Me.DataGridViewTextBoxColumn2.DataPropertyName = "RangoInferior"
        Me.DataGridViewTextBoxColumn2.HeaderText = "RangoInferior"
        Me.DataGridViewTextBoxColumn2.Name = "DataGridViewTextBoxColumn2"
        Me.DataGridViewTextBoxColumn2.ReadOnly = True
        Me.DataGridViewTextBoxColumn2.Width = 150
        '
        'DataGridViewTextBoxColumn3
        '
        Me.DataGridViewTextBoxColumn3.DataPropertyName = "RangoSuperior"
        Me.DataGridViewTextBoxColumn3.HeaderText = "RangoSuperior"
        Me.DataGridViewTextBoxColumn3.Name = "DataGridViewTextBoxColumn3"
        Me.DataGridViewTextBoxColumn3.ReadOnly = True
        Me.DataGridViewTextBoxColumn3.Width = 150
        '
        'CveRangoTextBox
        '
        Me.CveRangoTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraCatalogoDeRangosBindingSource, "CveRango", True))
        Me.CveRangoTextBox.Location = New System.Drawing.Point(217, 159)
        Me.CveRangoTextBox.Name = "CveRangoTextBox"
        Me.CveRangoTextBox.ReadOnly = True
        Me.CveRangoTextBox.Size = New System.Drawing.Size(100, 20)
        Me.CveRangoTextBox.TabIndex = 14
        Me.CveRangoTextBox.TabStop = False
        '
        'RangoInferiorTextBox
        '
        Me.RangoInferiorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraCatalogoDeRangosBindingSource, "RangoInferior", True))
        Me.RangoInferiorTextBox.Location = New System.Drawing.Point(217, 185)
        Me.RangoInferiorTextBox.Name = "RangoInferiorTextBox"
        Me.RangoInferiorTextBox.ReadOnly = True
        Me.RangoInferiorTextBox.Size = New System.Drawing.Size(100, 20)
        Me.RangoInferiorTextBox.TabIndex = 15
        Me.RangoInferiorTextBox.TabStop = False
        '
        'RangoSuperiorTextBox
        '
        Me.RangoSuperiorTextBox.DataBindings.Add(New System.Windows.Forms.Binding("Text", Me.MuestraCatalogoDeRangosBindingSource, "RangoSuperior", True))
        Me.RangoSuperiorTextBox.Location = New System.Drawing.Point(217, 211)
        Me.RangoSuperiorTextBox.Name = "RangoSuperiorTextBox"
        Me.RangoSuperiorTextBox.ReadOnly = True
        Me.RangoSuperiorTextBox.Size = New System.Drawing.Size(100, 20)
        Me.RangoSuperiorTextBox.TabIndex = 16
        Me.RangoSuperiorTextBox.TabStop = False
        '
        'BrwRangos
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(734, 566)
        Me.Controls.Add(Me.MuestraCatalogoDeRangosDataGridView)
        Me.Controls.Add(RangoSuperiorLabel)
        Me.Controls.Add(Me.RangoSuperiorTextBox)
        Me.Controls.Add(RangoInferiorLabel)
        Me.Controls.Add(Me.RangoInferiorTextBox)
        Me.Controls.Add(CveRangoLabel)
        Me.Controls.Add(Me.CveRangoTextBox)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "BrwRangos"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Catálogo de Rangos"
        CType(Me.DataSetEDGAR, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraCatalogoDeRangosBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.MuestraCatalogoDeRangosDataGridView, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents Button3 As System.Windows.Forms.Button
    Friend WithEvents DataSetEDGAR As sofTV.DataSetEDGAR
    Friend WithEvents MuestraCatalogoDeRangosBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents MuestraCatalogoDeRangosTableAdapter As sofTV.DataSetEDGARTableAdapters.MuestraCatalogoDeRangosTableAdapter
    Friend WithEvents MuestraCatalogoDeRangosDataGridView As System.Windows.Forms.DataGridView
    Friend WithEvents CveRangoTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RangoInferiorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents RangoSuperiorTextBox As System.Windows.Forms.TextBox
    Friend WithEvents DataGridViewTextBoxColumn1 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn2 As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DataGridViewTextBoxColumn3 As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
