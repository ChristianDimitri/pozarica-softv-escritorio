<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class FrmLoginPremium
    Inherits System.Windows.Forms.Form

    'Form reemplaza a Dispose para limpiar la lista de componentes.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Requerido por el Diseñador de Windows Forms
    Private components As System.ComponentModel.IContainer

    'NOTA: el Diseñador de Windows Forms necesita el siguiente procedimiento
    'Se puede modificar usando el Diseñador de Windows Forms.  
    'No lo modifique con el editor de código.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(FrmLoginPremium))
        Me.CMBClv_UsuarioLabel = New System.Windows.Forms.Label
        Me.CMBPasswordLabel = New System.Windows.Forms.Label
        Me.Cancel = New System.Windows.Forms.Button
        Me.OK = New System.Windows.Forms.Button
        Me.PasaporteTextBox = New System.Windows.Forms.TextBox
        Me.Clv_UsuarioTextBox = New System.Windows.Forms.TextBox
        Me.redLabel1 = New System.Windows.Forms.Label
        Me.LogoPictureBox = New System.Windows.Forms.PictureBox
        Me.DataSetEric = New sofTV.DataSetEric
        Me.DAMECLAVEUSUARIOBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.DAMECLAVEUSUARIOTableAdapter = New sofTV.DataSetEricTableAdapters.DAMECLAVEUSUARIOTableAdapter
        Me.VerAcceso_ChecaActivaBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.VerAcceso_ChecaActivaTableAdapter = New sofTV.DataSetEricTableAdapters.VerAcceso_ChecaActivaTableAdapter
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DAMECLAVEUSUARIOBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.VerAcceso_ChecaActivaBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'CMBClv_UsuarioLabel
        '
        Me.CMBClv_UsuarioLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBClv_UsuarioLabel.Location = New System.Drawing.Point(286, 40)
        Me.CMBClv_UsuarioLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBClv_UsuarioLabel.Name = "CMBClv_UsuarioLabel"
        Me.CMBClv_UsuarioLabel.Size = New System.Drawing.Size(149, 17)
        Me.CMBClv_UsuarioLabel.TabIndex = 35
        Me.CMBClv_UsuarioLabel.Text = "Login :"
        Me.CMBClv_UsuarioLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'CMBPasswordLabel
        '
        Me.CMBPasswordLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.CMBPasswordLabel.Location = New System.Drawing.Point(286, 108)
        Me.CMBPasswordLabel.Margin = New System.Windows.Forms.Padding(4, 0, 4, 0)
        Me.CMBPasswordLabel.Name = "CMBPasswordLabel"
        Me.CMBPasswordLabel.Size = New System.Drawing.Size(149, 16)
        Me.CMBPasswordLabel.TabIndex = 34
        Me.CMBPasswordLabel.Text = "Password :"
        Me.CMBPasswordLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'Cancel
        '
        Me.Cancel.BackColor = System.Drawing.Color.Orange
        Me.Cancel.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.Cancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.Cancel.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Cancel.Location = New System.Drawing.Point(412, 207)
        Me.Cancel.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.Cancel.Name = "Cancel"
        Me.Cancel.Size = New System.Drawing.Size(152, 35)
        Me.Cancel.TabIndex = 31
        Me.Cancel.Text = "&CANCELAR"
        Me.Cancel.UseVisualStyleBackColor = False
        '
        'OK
        '
        Me.OK.BackColor = System.Drawing.Color.Orange
        Me.OK.FlatStyle = System.Windows.Forms.FlatStyle.Flat
        Me.OK.Font = New System.Drawing.Font("Microsoft Sans Serif", 14.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.OK.Location = New System.Drawing.Point(249, 207)
        Me.OK.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.OK.Name = "OK"
        Me.OK.Size = New System.Drawing.Size(155, 35)
        Me.OK.TabIndex = 30
        Me.OK.Text = "&ACEPTAR"
        Me.OK.UseVisualStyleBackColor = False
        '
        'PasaporteTextBox
        '
        Me.PasaporteTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.PasaporteTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.PasaporteTextBox.Location = New System.Drawing.Point(288, 127)
        Me.PasaporteTextBox.MaxLength = 10
        Me.PasaporteTextBox.Name = "PasaporteTextBox"
        Me.PasaporteTextBox.PasswordChar = Global.Microsoft.VisualBasic.ChrW(42)
        Me.PasaporteTextBox.Size = New System.Drawing.Size(175, 24)
        Me.PasaporteTextBox.TabIndex = 29
        '
        'Clv_UsuarioTextBox
        '
        Me.Clv_UsuarioTextBox.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle
        Me.Clv_UsuarioTextBox.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper
        Me.Clv_UsuarioTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 11.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Clv_UsuarioTextBox.Location = New System.Drawing.Point(288, 60)
        Me.Clv_UsuarioTextBox.MaxLength = 15
        Me.Clv_UsuarioTextBox.Name = "Clv_UsuarioTextBox"
        Me.Clv_UsuarioTextBox.Size = New System.Drawing.Size(175, 24)
        Me.Clv_UsuarioTextBox.TabIndex = 28
        '
        'redLabel1
        '
        Me.redLabel1.BackColor = System.Drawing.Color.Transparent
        Me.redLabel1.CausesValidation = False
        Me.redLabel1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.redLabel1.Font = New System.Drawing.Font("Microsoft Sans Serif", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.redLabel1.ForeColor = System.Drawing.Color.Red
        Me.redLabel1.Location = New System.Drawing.Point(13, 9)
        Me.redLabel1.Name = "redLabel1"
        Me.redLabel1.Size = New System.Drawing.Size(220, 35)
        Me.redLabel1.TabIndex = 33
        Me.redLabel1.Text = "Acceso Restringido"
        Me.redLabel1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
        '
        'LogoPictureBox
        '
        Me.LogoPictureBox.Image = CType(resources.GetObject("LogoPictureBox.Image"), System.Drawing.Image)
        Me.LogoPictureBox.Location = New System.Drawing.Point(13, 47)
        Me.LogoPictureBox.Margin = New System.Windows.Forms.Padding(4, 3, 4, 3)
        Me.LogoPictureBox.Name = "LogoPictureBox"
        Me.LogoPictureBox.Size = New System.Drawing.Size(220, 195)
        Me.LogoPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage
        Me.LogoPictureBox.TabIndex = 32
        Me.LogoPictureBox.TabStop = False
        '
        'DataSetEric
        '
        Me.DataSetEric.DataSetName = "DataSetEric"
        Me.DataSetEric.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'DAMECLAVEUSUARIOBindingSource
        '
        Me.DAMECLAVEUSUARIOBindingSource.DataMember = "DAMECLAVEUSUARIO"
        Me.DAMECLAVEUSUARIOBindingSource.DataSource = Me.DataSetEric
        '
        'DAMECLAVEUSUARIOTableAdapter
        '
        Me.DAMECLAVEUSUARIOTableAdapter.ClearBeforeFill = True
        '
        'VerAcceso_ChecaActivaBindingSource
        '
        Me.VerAcceso_ChecaActivaBindingSource.DataMember = "VerAcceso_ChecaActiva"
        Me.VerAcceso_ChecaActivaBindingSource.DataSource = Me.DataSetEric
        '
        'VerAcceso_ChecaActivaTableAdapter
        '
        Me.VerAcceso_ChecaActivaTableAdapter.ClearBeforeFill = True
        '
        'FrmLoginPremiun
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(601, 295)
        Me.Controls.Add(Me.CMBClv_UsuarioLabel)
        Me.Controls.Add(Me.CMBPasswordLabel)
        Me.Controls.Add(Me.Cancel)
        Me.Controls.Add(Me.OK)
        Me.Controls.Add(Me.PasaporteTextBox)
        Me.Controls.Add(Me.Clv_UsuarioTextBox)
        Me.Controls.Add(Me.redLabel1)
        Me.Controls.Add(Me.LogoPictureBox)
        Me.Name = "FrmLoginPremiun"
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Acceso"
        CType(Me.LogoPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataSetEric, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DAMECLAVEUSUARIOBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.VerAcceso_ChecaActivaBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents CMBClv_UsuarioLabel As System.Windows.Forms.Label
    Friend WithEvents CMBPasswordLabel As System.Windows.Forms.Label
    Friend WithEvents Cancel As System.Windows.Forms.Button
    Friend WithEvents OK As System.Windows.Forms.Button
    Friend WithEvents PasaporteTextBox As System.Windows.Forms.TextBox
    Friend WithEvents Clv_UsuarioTextBox As System.Windows.Forms.TextBox
    Friend WithEvents redLabel1 As System.Windows.Forms.Label
    Friend WithEvents LogoPictureBox As System.Windows.Forms.PictureBox
    Friend WithEvents DataSetEric As sofTV.DataSetEric
    Friend WithEvents DAMECLAVEUSUARIOBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents DAMECLAVEUSUARIOTableAdapter As sofTV.DataSetEricTableAdapters.DAMECLAVEUSUARIOTableAdapter
    Friend WithEvents VerAcceso_ChecaActivaBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents VerAcceso_ChecaActivaTableAdapter As sofTV.DataSetEricTableAdapters.VerAcceso_ChecaActivaTableAdapter
End Class
